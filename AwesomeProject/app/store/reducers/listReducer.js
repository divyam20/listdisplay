import {GET_LIST, GET_AUTHOR} from '../types';

const InitialState = {
  list: [],
  filteredData: [],
};

export default (state = InitialState, action) => {
  switch (action.type) {
    case GET_LIST:
      return {
        ...state,
        list: [...action.payload, ...state.list],
      };
    case GET_AUTHOR:
      return {
        ...state,
        filteredData: [...action.payload],
      };
    default:
      return state;
  }
};
