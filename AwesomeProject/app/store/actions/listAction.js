import {GET_LIST, GET_AUTHOR, GET_TITLE} from '../types';

this.page = 1;

export const getListAction = (data) => ({
  type: GET_LIST,
  payload: data,
});

export const getList = () => async (dispatch) => {
  fetch(
    `https://hn.algolia.com/api/v1/search_by_date?tags=story&page=${this.page}`,
  )
    .then((response) => response.json())
    .then((responseJson) => {
      this.page = this.page + 1;
      dispatch(getListAction(responseJson.hits));
    });
};

export const getSearchAuthor = (data) => {
  console.log('DATA', data);
  return {
    type: GET_AUTHOR,
    payload: data,
  };
};
