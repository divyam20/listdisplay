import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  Button,
  Modal,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  getList,
  getSearchAuthor,
  getListAction,
} from '../store/actions/listAction';

import radionOn from '../../assets/images/radio_green_on.png';
import radioOff from '../../assets/images/radio_green_off.png';

const filterList = [
  {name: 'Author'},
  {name: 'Title'},
  {name: 'Date ASC'},
  {name: 'Date DESC'},
];

const searchList = [{name: 'Author'}, {name: 'Title'}];

const filterAuthor = (a, b) => {
  if (
    a.author.trim().substring(0, 1).toLowerCase() <
    b.author.trim().substring(0, 1).toLowerCase()
  ) {
    return -1;
  }
  if (
    a.author.trim().substring(0, 1).toLowerCase() >
    b.author.trim().substring(0, 1).toLowerCase()
  ) {
    return 1;
  }
  return 0;
};

const filterTitle = (a, b) => {
  if (
    a.title.trim().substring(0, 1).toLowerCase() <
    b.title.trim().substring(0, 1).toLowerCase()
  ) {
    return -1;
  }
  if (
    a.title.trim().substring(0, 1).toLowerCase() >
    b.title.trim().substring(0, 1).toLowerCase()
  ) {
    return 1;
  }
  return 0;
};

const filterDate = (a, b) => {
  if (new Date(a.created_at) < new Date(b.created_at)) {
    return -1;
  }
  if (new Date(a.created_at) > new Date(b.created_at)) {
    return 1;
  }
  return 0;
};

const filterDateDesc = (a, b) => {
  if (new Date(a.created_at) > new Date(b.created_at)) {
    return -1;
  }
  if (new Date(a.created_at) < new Date(b.created_at)) {
    return 1;
  }
  return 0;
};

const DisplayList = () => {
  const dispatch = useDispatch();
  const {list, filteredData} = useSelector((state) => state.listReducer);
  const [filterEnable, setFilter] = useState(false);
  const [filterId, setFilterId] = useState(0);
  const [searchEnable, setSearch] = useState(false);
  const [searchId, setSearchId] = useState(0);
  const [searchText, setSearchText] = useState('');
  const [dialogDisplay, setDialog] = useState(false);
  const [itemDisplay, setItem] = useState('');

  useEffect(() => {
    setTimeout(function () {
      dispatch(getList(filterId));
    }, 3000);
    if (filterId === 0) {
      list.sort((a, b) => filterAuthor(a, b));
    } else if (filterId === 1) {
      list.sort((a, b) => filterTitle(a, b));
    } else if (filterId === 2) {
      list.sort((a, b) => filterDate(a, b));
    } else if (filterId === 3) {
      list.sort((a, b) => filterDateDesc(a, b));
    }
  });

  const dialog = () => {
    return (
      <Modal visible={dialogDisplay} onRequestClose={() => {}} transparent>
        <View style={styles.dialogContainer}>
          <View style={styles.dialogView}>
            <Text style={styles.dialogTitle}>
              {`Title: ${itemDisplay.title}`}
            </Text>
            <Text
              style={styles.dialogText}>{`Author: ${itemDisplay.author}`}</Text>
            <Text style={styles.dialogText}>{`Link: ${itemDisplay.url}`}</Text>
            <Button title="CLOSE" onPress={() => setDialog(false)} />
          </View>
        </View>
      </Modal>
    );
  };

  const renderItem = ({item, index}) => {
    const options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    };
    const dateDisplayed = new Date(item.created_at).toLocaleDateString(
      undefined,
      options,
    );
    return (
      <TouchableOpacity
        onPress={() => {
          setDialog(true);
          setItem(item);
        }}>
        <View style={styles.childOuterView} key={index}>
          <Text style={styles.childText}>
            <Text style={styles.title}>Title</Text>
            {` ${item.title}`}
          </Text>
          <Text style={styles.childText}>
            <Text style={styles.title}>Link</Text>
            {` ${item.url}`}
          </Text>
          <Text style={styles.childText}>
            <Text style={styles.title}>Author</Text>
            {` ${item.author}`}
          </Text>
          <Text style={styles.childText}>
            <Text style={styles.title}>Date</Text>
            {` ${dateDisplayed}`}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <TouchableOpacity
          onPress={() => {
            setSearch(!searchEnable);
            setFilter(false);
          }}>
          <View style={styles.SearchView}>
            <Text>
              {searchEnable ? 'Close Search Options' : 'Open Search Options'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setFilter(!filterEnable);
            setSearch(false);
          }}>
          <View style={styles.filterView}>
            <Text>
              {filterEnable ? 'Close Filter Options' : 'Open Filter Options'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {filterEnable && (
        <View>
          <Text>Filter By</Text>
          {filterList.map((item, index) => (
            <View style={styles.searchContainer}>
              <TouchableOpacity
                onPress={() => {
                  setFilterId(index);
                  if (filterId === 0) {
                    list.sort((a, b) => filterAuthor(a, b));
                  } else if (filterId === 1) {
                    list.sort((a, b) => filterTitle(a, b));
                  } else if (filterId === 2) {
                    list.sort((a, b) => filterDate(a, b));
                  } else if (filterId === 3) {
                    list.sort((a, b) => filterDateDesc(a, b));
                  }
                }}>
                <Image source={filterId === index ? radionOn : radioOff} />
              </TouchableOpacity>
              <Text style={{marginLeft: 10, marginTop: 5}}>{item.name}</Text>
            </View>
          ))}
        </View>
      )}
      {searchEnable && (
        <View>
          <TextInput
            style={styles.searchBoxView}
            onChangeText={(text) => setSearchText(text)}
          />
          <View style={styles.searchListType}>
            {searchList.map((item, index) => (
              <View style={styles.searchContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setSearchId(index);
                  }}>
                  <Image source={searchId === index ? radionOn : radioOff} />
                </TouchableOpacity>
                <Text style={{marginLeft: 10, marginTop: 5}}>{item.name}</Text>
              </View>
            ))}
          </View>
          <Button
            title="Start Search"
            onPress={() => {
              if (searchId === 0) {
                dispatch(
                  getSearchAuthor(
                    list.filter((item) =>
                      item.author
                        .toLowerCase()
                        .includes(searchText.toLowerCase()),
                    ),
                  ),
                );
              } else if (searchId === 1) {
                dispatch(
                  getSearchAuthor(
                    list.filter((item) =>
                      item.title
                        .toLowerCase()
                        .includes(searchText.toLowerCase()),
                    ),
                  ),
                );
              }
            }}
          />
        </View>
      )}
      <FlatList
        data={searchEnable ? filteredData : list}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item) => renderItem(item)}
        onRefresh={() => dispatch(getListAction(list))}
        refreshing={false}
      />
      {dialog()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    marginTop: 10,
  },
  childOuterView: {
    width: '90%',
    alignSelf: 'center',
    flex: 1,
    backgroundColor: 'white',
    elevation: 1,
    shadowColor: '#d2d2d2',
    shadowOffset: {width: 0, height: 1},
    shadowRadius: 3,
    marginTop: 5,
    marginBottom: 5,
  },
  childText: {
    marginTop: 5,
    marginBottom: 5,
  },
  title: {
    fontWeight: 'bold',
  },
  filterView: {
    alignSelf: 'flex-end',
    marginRight: 10,
    marginTop: 10,
  },
  SearchView: {
    alignSelf: 'flex-start',
    marginLeft: 10,
    marginTop: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  searchBoxView: {
    alignSelf: 'center',
    width: '60%',
    height: 40,
    borderColor: '#d2d2d2',
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#fff',
    marginTop: 10,
  },
  searchListType: {
    flexDirection: 'row',
    marginTop: 10,
  },
  dialogContainer: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialogView: {
    backgroundColor: 'white',
    width: '80%',
    height: '30%',
    shadowColor: '#d2d2d2',
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 3,
    shadowOffset: {width: 0, height: 1},
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialogTitle: {
    fontWeight: '600',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  dialogText: {
    marginTop: 10,
    textAlign: 'center',
  },
});

export default DisplayList;
