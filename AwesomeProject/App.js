import * as React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import DisplayList from './app/src/DisplayList';

import store from './app/store';
import {Provider} from 'react-redux';

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={DisplayList} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
